### step 0 : get fresh ci scripts 
```bash
docker pull quay.io/fravi/ci-imagebuilder:latest
docker run --rm -ti  -v `pwd`/ci:/target quay.io/fravi/ci-imagebuilder:latest
```

### step 1: extract tag and version from git 
  ./ci/buildCurrentVersionName.sh

### step 2:
   
```bash
IMAGENAME="docker-fatubuntu-jdk" DOCKER_REPO="quay.io/fravi" PUSH_SHA=0 PUSH_LATEST=1 PUSH_TAG=1 ./ci/buildImage.sh

```

